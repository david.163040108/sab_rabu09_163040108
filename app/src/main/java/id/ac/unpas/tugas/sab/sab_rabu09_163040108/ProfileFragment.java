package id.ac.unpas.tugas.sab.sab_rabu09_163040108;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    public  static  MainActivity mainActivity;

    public static ProfileFragment newInstance(MainActivity activity) {
        // Required empty public constructor
        mainActivity = activity;
        return  new ProfileFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvNRP);
        TextView tvNama = (TextView) view.findViewById(R.id.tvNama);
        TextView tvPraktikum = (TextView) view.findViewById(R.id.tvSab);
        TextView tvShift = (TextView) view.findViewById(R.id.tvShift);

        String nrp = getActivity().getIntent().getExtras().getString("nrp");
        String nama = getActivity().getIntent().getExtras().getString("name");
        String praktikum = getActivity().getIntent().getExtras().getString("praktikum");
        String shift = getActivity().getIntent().getExtras().getString("shift");

        tvTitle.setText(nrp);
        tvNama.setText(nama);
        tvPraktikum.setText(praktikum);
        tvShift.setText(shift);
        return  view;
    }

}
