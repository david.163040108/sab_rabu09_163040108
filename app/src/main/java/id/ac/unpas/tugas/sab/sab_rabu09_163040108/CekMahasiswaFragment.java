package id.ac.unpas.tugas.sab.sab_rabu09_163040108;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class CekMahasiswaFragment extends Fragment {

    EditText nrp,nama;
    Button btnSubmit;

    public CekMahasiswaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_cek_mahasiswa, container, false);
        Button btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
        nrp = (EditText) view.findViewById(R.id.etNRP);
        nama = (EditText) view.findViewById(R.id.etNama);



        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nrp2 = nrp.getText().toString();
                String nama2 = nama.getText().toString();
                if (nrp2.trim().equalsIgnoreCase("")){
                    nrp.setError("NRP tidak boleh kosong");
                    nrp.requestFocus();
                }else if (nama2.trim().equalsIgnoreCase("")) {
                    nama.setError("Nama tidak boleh kosong");
                    nama.requestFocus();
                }else {

                    if (cekNrp(nrp.getText().toString())) {
                        Toast.makeText(getActivity(), "Hello SAB, perkenalan saya " +
                                nama.getText() + " dengan nrp " + nrp.getText() + " adalah mahasiswa Teknik Informatika Unpas", Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(getActivity(), "Hello SAB, perkenalan saya " +
                                nama.getText() + " dengan nrp " + nrp.getText() + " adalah bukan mahasiswa Teknik Informatika ", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return  view;
    }

    //cek Nrp
    public boolean cekNrp(String nrp){
        if (nrp.substring(2,5).equals("304")){
            return  true;
        }
        return false;
    }

}
