package id.ac.unpas.tugas.sab.sab_rabu09_163040108;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {


    public  static  MainActivity mainActivity;
    public static ContactFragment newInstance(MainActivity activity) {
        mainActivity = activity;
        return new ContactFragment();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_contact, container, false);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        return view;
    }

}
