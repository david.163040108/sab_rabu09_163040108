package id.ac.unpas.tugas.sab.sab_rabu09_163040108;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText editTextUsername, editTextPassword;
    Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = (EditText) findViewById(R.id.etUsername);
        editTextPassword = (EditText) findViewById(R.id.etPassword);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();
                if (username.trim().equalsIgnoreCase("")){
                    editTextUsername.setError("Username tidak boleh kosong");
                    editTextUsername.requestFocus();
                }else if (password.trim().equalsIgnoreCase("")){
                    editTextPassword.setError("Password tidak boleh kosong");
                    editTextPassword.requestFocus();
                }else {
                    if (username.equalsIgnoreCase("163040108") && password.equalsIgnoreCase("admin")){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("nrp", username);
                        intent.putExtra("name","David Priyadi");
                        intent.putExtra("praktikum","Praktikum SAB");
                        intent.putExtra("shift", "Shift Selasa 09.40");
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }else if (username.equalsIgnoreCase("Asdos") && password.equalsIgnoreCase("Asdos")){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("nrp", username);
                        intent.putExtra("name","Asisten SAB");
                        intent.putExtra("praktikum","Praktikum SAB");
                        intent.putExtra("shift", "Shift Selasa 09.40");
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }
                    else {
                        Toast.makeText(LoginActivity.this,"Username dan Password tidak sesuai", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


    }
}
