package id.ac.unpas.tugas.sab.sab_rabu09_163040108;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter{

    final int PAGE_COUNT = 5;
    private String tebTitles[] = new String[] {"Profile","Gallery","Contact","Cek Mahasiwa","Setting"};

    public SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
       switch (i){
           case 0 :
               ProfileFragment tab1 = new ProfileFragment();

               return  tab1;
           case 1 :
               GalleryFragment tab2 = new GalleryFragment();
               return  tab2;
           case 2 :
               ContactFragment tab3 = new ContactFragment();
               return  tab3;
           case 3 :
               CekMahasiswaFragment tab4 = new CekMahasiswaFragment();
               return  tab4;
           case 4 :
               SettingFragment tab5 = new SettingFragment();
               return tab5;
               default:
                   return null;
       }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return  tebTitles[position];
    }


}
